package calculadora;

/**
 *
 * @author Juan Carlos
 */
public class Operaciones {

  private Arbol arbol;

  public Operaciones(Arbol arbol) {
    this.arbol = arbol;
  }

  public int resolverArbol(){
        return resolverArbol2(arbol);
  }
    
    private int resolverArbol2(Arbol arbol) {
        int res;
        if (arbol.esHoja()) {
            res = Integer.parseInt(arbol.getRaiz());
        } else {
            switch (arbol.getRaiz()) {
                case "*":
                    res = resolverArbol2(arbol.getHizoIzq()) * resolverArbol2(arbol.getHizoDer());
                    break;
                case "/":
                    res = resolverArbol2(arbol.getHizoIzq()) / resolverArbol2(arbol.getHizoDer());
                    break;
                case "+":
                    res = resolverArbol2(arbol.getHizoIzq()) + resolverArbol2(arbol.getHizoDer());
                    break;
                default:
                    res = resolverArbol2(arbol.getHizoIzq()) - resolverArbol2(arbol.getHizoDer());
                    break;
            }
        }
        return res;
    }
}
