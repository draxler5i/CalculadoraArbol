package calculadora;

import java.util.Scanner;

/**
 *
 * @author Juan Carlos
 */

public class Calculadora {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    System.out.println("Ingresar ecuacion");

    String ecuacion = sc.nextLine();

    String [] separados = ecuacion.split(" ");

    ConstruirArbol contructor = new ConstruirArbol(separados);

    Arbol arbol = contructor.construir();

    System.out.println("Arbol: "+arbol.toString());

    System.out.println("Hijos: "+arbol.getHijosString());
    
    Operaciones operacion = new Operaciones(arbol);

    int total = operacion.resolverArbol();

    System.out.println(total);
  }
}
