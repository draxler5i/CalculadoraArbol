package calculadora;

import java.util.ArrayList;

/**
 *
 * @author Juan Carlos
 */
public class Arbol {
  
  private String raiz;
  private ArrayList<Arbol> hijos;

  public Arbol(String raiz) {
    this.raiz = raiz;
    hijos = new ArrayList<>();
  }
  
  public void aniadirHijo(Arbol hijo) {
    hijos.add(hijo);
  }

  public void aniadirHijoIzq(Arbol hijo) {
    hijos.add(0,hijo);
  }

  public String getRaiz() {
    return raiz;
  }

  public boolean esHoja(){
    return hijos.isEmpty();
  }

  public String toString() {
    return ""+raiz;
  }

  public String getHijosString() {
    return hijos.toString();
  }

  public Arbol getHizoIzq() {
    return hijos.get(0);
  }

  public Arbol getHizoDer() {
    return hijos.get(1);
  }
}
