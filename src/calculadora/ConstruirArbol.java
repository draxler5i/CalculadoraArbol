package calculadora;

/**
 *
 * @author Juan Carlos
 */
public class ConstruirArbol {
  
  private String[] arreglo;

  public ConstruirArbol(String[] arreglo) {
    this.arreglo = arreglo;
  }

  public Arbol construir() {
    return construir2(0);
  }

  private Arbol construir2(int i) {
    Arbol res;
        if (!esOperacion(arreglo[i])) {
            if (i == arreglo.length - 1) {
                res = new Arbol(arreglo[i]);
                return res;
            }
            Arbol arbol = new Arbol(arreglo[i]);
            res = construir2(i+1);
            res.aniadirHijoIzq(arbol);
            return res;
        } else {
            res = new Arbol(arreglo[i]);
            Arbol arbol = construir2(i+1);
            res.aniadirHijo(arbol);
        }
        return res;
  }

  private boolean esOperacion(String cadena) {
    boolean res = false;
    if (cadena.equals("+")) {
      res = true;
    }
    if (cadena.equals("-")) {
      res = true;
    }
    if (cadena.equals("*")) {
      res = true;
    }
    if (cadena.equals("/")) {
      res = true;
    }
    return res;
  }
}
